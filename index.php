<?php

session_start();
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));

$params_url = explode('/', $_GET['p']);
$controller = $params_url[0];
$action = $params_url[1];
$params = array_slice($params_url, 2);

require ROOT . 'controllers/' . $controller . '.php';

$controller_object = new $controller();
if (method_exists($controller_object, $action)) {
    call_user_func_array(array($controller_object, $action), $params);
} else {
    echo 'ERROR 404 : Page not found';
}
?>