<?php

class articles {

    private $model;

    public function __construct() {
        if (!isset($_SESSION['User'])) {
            header("Location:/phpws1/users/login");
            exit();
        } else {
            require(ROOT . 'models/articlesModel.php');
            $this->model = new articlesModel();
        }
    }

    public function liste() {
        $liste = $this->model->get();
        include ROOT . 'views/articles/liste.php';
    }

    public function add() {
        if (isset($_POST['newArticle'])) {
            $title = isset($_POST['titleSaisi']) ? $_POST['titleSaisi'] : null;
            $content = isset($_POST['contentSaisi']) ? $_POST['contentSaisi'] : null;
            $this->model->save($title, $content);
            header("Location:/phpws1/articles/liste");
            exit();
        }
        include ROOT . 'views/articles/add.php';
    }

    public function delete($id) {
        $this->model->delete($id);
        header("Location:/phpws1/articles/liste");
        exit();
        include ROOT . 'views/articles/add.php';
    }

}

?>
