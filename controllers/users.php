<?php

class users {

    private $model;

    public function __construct() {
        require(ROOT . 'models/usersModel.php');
        $this->model = new usersModel();
    }

    public function login() {
        echo 'Je suis dans la méthode logIN de la classe users.<br>';
        if (isset($_POST['valider'])) {
            $login = isset($_POST['login']) ? $_POST['login'] : null;
            $password = isset($_POST['password']) ? sha1($_POST['password']) : null;

            $user = $this->model->get($login);

            if ($user) {
                if ($user['password'] == $password) {
                    echo "Autorisation d'accès au site";
                    $_SESSION['User'] = $login;
                    header("Location:/phpws1/articles/liste");
                    exit();
                } else {
                    echo "Login ou mot de passe incorrect";
                }
            } else {
                echo "Le login selectionné n'existe pas";
            }
        }
        include ROOT . 'views/users/login.php';
    }

    public function logout() {
        echo 'Je suis dans la méthode logOUT de la classe users.';
        unset($_SESSION['User']);
        include ROOT . 'views/users/login.php';
    }

    public function subscribe() {
        echo "Je suis dans la méthode subscribe de la classe users.";

        $login = isset($_POST['loginSaisi']) ? $_POST['loginSaisi'] : null;
        $password = isset($_POST['passwordSaisi']) ? $_POST['passwordSaisi'] : null;
        $this->model->save($login, $password);
    }

}

?>
