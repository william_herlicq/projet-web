<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Liste des articles</h1>
        <a href="/phpws1/articles/add">Ajouter un article</a>


        <table>
            <?php
            foreach ($liste as $element) {
                ?>
                <tr>
                    <td>Article <?php echo $element['id']; ?> </td>
                    <td><?php echo $element['title']; ?></td>
                    <td><?php echo $element['content']; ?></td>
                    <td>
                        <a href="/phpws1/articles/delete/<?php echo $element['id']; ?>" >Supprimer</a>                        
                    </td>
                </tr>
            <?php }
            ?>
        </table>

        <p>Utilisateur connecté : <a href="/phpws1/users/logout">Logout</a></p>
    </body>
</html>