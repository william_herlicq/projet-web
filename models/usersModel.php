<?php

class usersModel {

    function __construct() {
        //connexion a la BDD

        $host = 'localhost'; // Nom du server SQL
        $user = 'root'; // Votre nom d'utilisateur SQL
        $pass = ''; // Votre mot de passe SQL
        $database = 'phpws1'; // Votre BDD

        $dsn = 'mysql:host=' . $host . ';dbname=' . $database;


        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
            $this->dbh = new PDO($dsn, $user, $pass, $pdo_options);
            echo 'Connexion réussie';
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public function get($login) {
        //recherche du login
        try {

            $sql = "SELECT * FROM `users` WHERE login = '" . $login . "'";
            $sth = $this->dbh->query($sql);
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);

            $sth->closeCursor();
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $result[0];
    }

    public function save($login, $password) {
        //Enregistrer d'un nouvel utilisateur
        try {
            $login = $this->dbh->quote($login);
            $password = $this->dbh->quote(sha1($password));

            // Insertion d’un enregistrement
            $sql = "INSERT INTO users (login, password)
	VALUES ($login, $password)";
            $this->dbh->exec($sql);
            
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

}

?>
