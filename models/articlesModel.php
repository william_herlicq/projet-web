<?php

class articlesModel{
    
    function __construct() {
        //connexion a la BDD

        $host = 'localhost'; // Nom du server SQL
        $user = 'root'; // Votre nom d'utilisateur SQL
        $pass = ''; // Votre mot de passe SQL
        $database = 'phpws1'; // Votre BDD

        $dsn = 'mysql:host=' . $host . ';dbname=' . $database;


        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
            $this->dbh = new PDO($dsn, $user, $pass, $pdo_options);
            echo 'Connexion réussie';
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
    
    function get() {
        try {

            $sql = "SELECT * FROM `articles`";
            $sth = $this->dbh->query($sql);
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);

            $sth->closeCursor();
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $result;
    }
        
    public function save($title, $content) {
        //Enregistrer d'un nouvel utilisateur
        try {
            $title = $this->dbh->quote($title);
            $content = $this->dbh->quote($content);

            // Insertion d’un enregistrement
            $sql = "INSERT INTO articles (title, content)
	VALUES ($title, $content)";
            $this->dbh->exec($sql);
            
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
        
    public function delete($id) {
        //Enregistrer d'un nouvel utilisateur
        try {
            // Insertion d’un enregistrement
            $sql = "DELETE FROM articles WHERE id = $id";
            $this->dbh->exec($sql);
            
        } catch (PDOException $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
}
?>
